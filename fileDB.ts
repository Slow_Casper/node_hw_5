import path from "path";
import fsPromise from 'fs/promises';
import { Table } from "./Table";

interface Schema {
    [key: string]: StringConstructor | NumberConstructor | DateConstructor;
}

interface Command {
    execute(): Promise<void>;
}

class RegisterSchemaCommand implements Command {
    private tableName: string;
    private schema: Schema;

    constructor(tableName: string, schema: Schema) {
        this.tableName = tableName;
        this.schema = schema;
    }

    async execute() {
        const filePath = path.join(__dirname, `files/${this.tableName}.json`);
        await fsPromise.writeFile(filePath, '[]');
        console.log(`Schema for table ${this.tableName} has registered!`);
    }
}

class FileDB {
    private schemas: { [tableName: string]: Schema };

    constructor() {
        this.schemas = {};
    }

    async executeCommand(command: Command) {
        try {
            await command.execute();
        } catch (error) {
            console.error(error);
        }
    }

    async registerSchema(tableName: string, schema: Schema) {
        const command = new RegisterSchemaCommand(tableName, schema);
        await this.executeCommand(command);
        this.schemas[tableName] = schema;
    }

    getTable(tableName: string) {
        const filePath = path.join(__dirname, `files/${tableName}.json`);
        return new Table(tableName, filePath);
    }
}

export default FileDB;